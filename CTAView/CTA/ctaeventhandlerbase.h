#ifndef CTAEVENTHANDLERBASE_H
#define CTAEVENTHANDLERBASE_H

#include <string>
#include <list>

#include "ctasimulationtype.h"
#include "ctadirection.h"
#include "ctaevent.h"

class CTAInitValues
{
private:
    int horizontalCount;
    int verticalCount;
    int blockSize;
    int blockLanes;
    eSimulationType simulationType;
    eDirection hDirectionFirstStreet;
    eDirection vDirectionFirstStreet;
    unsigned long interval;
    unsigned int carSpeed;

public:
    CTAInitValues(int HorizontalCount = 10, int VerticalCount = 10,
                  int BlockSize = 5, int BlockLanes = 2,
                    eSimulationType SimulationType = stSimplified,
                    eDirection HDirectionFirstStreet = gdBeginToEnd,
                    eDirection VDirectionFirstStreet = gdBeginToEnd,
                    unsigned long Interval = 1000, unsigned int CarSpeed = 1);

    int getHorizontalCount() const;
    int getVerticalCount() const;
    eSimulationType getSimulationType() const;
    eDirection getHDirectionFirstStreet() const;
    eDirection getVDirectionFirstStreet() const;
    unsigned long getInterval() const;
    int getBlockSize() const;
    int getBlockLanes() const;
    unsigned int getCarSpeed() const;
};

class CTASimulator;

class CTAEventHandlerBase
{
private:

protected:
    virtual CTAInitValues *getInitValuesInternal()=0;
    virtual void HandleEventsInternal(CTASimulator *simulator)=0;
public:
    CTAEventHandlerBase();
    CTAInitValues *getInitValues();
    void HandleEvents(CTASimulator *simulator);


};

typedef CTAInitValues *PCTAInitValues;
typedef CTAEventHandlerBase *PCTAEventHandlerBase;

#endif // CTAEVENTHANDLERBASE_H
