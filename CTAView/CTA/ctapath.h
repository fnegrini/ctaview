#ifndef CTAPATH_H
#define CTAPATH_H

#include "ctabase.h"
#include "ctapathstatus.h"

class CTACar;

class CTAPathFull;

class CTAPath: public CTABase
{
private:
    CTACar*** matrix;
    int horCount;
    int verCount;

    CTAPath *pathUp;
    CTAPath *pathDown;
    CTAPath *pathLeft;
    CTAPath *pathRight;

    eDirection horDirection;
    eDirection verDirection;

    ePathStatus horStatus;
    ePathStatus verStatus;

    bool revertNewLane;

protected:
    virtual int getHorOffSetInternal()=0;
    virtual int getVerOffSetInternal()=0;

public:
    CTAPath(std::string Name, CTAInterfaceBase *Log);
    void InitializePath(int HorCount, int VerCount);
    CTACar *GetPosition(int HorPos, int VerPos);
    bool CheckPosition(int HorPos, int VerPos);
    bool SetPosition(CTACar *Car, int HorPos, int VerPos);

    int getHorCount() const;
    int getVerCount() const;

    int getHorOffSet();
    int getVerOffSet();

    CTAPath *getPathUp() const;
    void setPathUp(CTAPath *value);

    CTAPath *getPathDown() const;
    void setPathDown(CTAPath *value);

    CTAPath *getPathLeft() const;
    void setPathLeft(CTAPath *value);

    CTAPath *getPathRight() const;
    void setPathRight(CTAPath *value);

    eDirection getHorDirection() const;
    void setHorDirection(const eDirection &value);

    eDirection getVerDirection() const;
    void setVerDirection(const eDirection &value);

    ePathStatus getHorStatus() const;
    void setHorStatus(const ePathStatus &value);

    ePathStatus getVerStatus() const;
    void setVerStatus(const ePathStatus &value);

    CTAPathFull *GetNextPath(int HorPos, int VerPos, eCarDirectioon Direction);
    CTAPathFull *GetFreeFirstPos(eCarDirectioon Direction);

};

class CTAPathFull
{
private:
    CTAPath *path;
    int horPos;
    int verPos;

public:
    CTAPathFull(CTAPath *Path = nullptr, int HorPos = -1, int VerPos = -1);

    CTAPath *getPath() const;
    int getHorPos() const;
    int getVerPos() const;
};

typedef CTAPath *PCTAPath;
typedef CTAPathFull *PCTAPathFull;

#endif // CTAPATH_H
