#include "ctablock.h"
#include "ctastreet.h"
#include "ctacar.h"

eOrientation CTABlock::GetOrientation()
{
    if(street == nullptr)
        return oNone;
    else
        return street->GetOrientation();
}

eDirection CTABlock::GetDirection()
{
    if(street == nullptr)
        return gdNone;
    else
        return street->GetDirection();
}



int CTABlock::getHorOffSetInternal()
{
    int result = 0;

    if(street != nullptr){

        switch (street->GetOrientation()) {
        case sdHorizontal:
            result = blockCount * (log->getBlockSize() + log->getBlockLanes());
            break;

        case sdVertical:
            result = ((street->getStreetCount()+1)*log->getBlockSize()) + (street->getStreetCount()*log->getBlockLanes());
            break;

        default:
            result = 0;
            break;
        }
    }
    return result;
}

int CTABlock::getVerOffSetInternal()
{
    int result = 0;

    if(street != nullptr){

        switch (street->GetOrientation()) {
        case sdHorizontal:
            result = ((street->getStreetCount()+1)*log->getBlockSize()) + (street->getStreetCount()*log->getBlockLanes());
            break;

        case sdVertical:
            result = blockCount * (log->getBlockSize() + log->getBlockLanes());
            break;

        default:
            result = 0;
            break;
        }
    }
    return result;
}

std::string CTABlock::GetClassNameInternal()
{
    return "Block";
}

CTABlock::CTABlock(std::string Name, CTAInterfaceBase *Log, int BlockCount, CTAStreet *Street)
    :CTAPath(Name, Log)
{
    street = Street;
    blockCount = BlockCount;

    if(street != nullptr){
        switch (street->GetOrientation()) {
        case sdHorizontal:
            setHorDirection(street->GetDirection());
            InitializePath(log->getBlockSize(), log->getBlockLanes());
            break;
        case sdVertical:
            setVerDirection(street->GetDirection());
            InitializePath(log->getBlockLanes(), log->getBlockSize());
            break;
        }

    }

}

CTABlock::~CTABlock()
{

}

