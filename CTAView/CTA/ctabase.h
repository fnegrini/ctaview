#ifndef CTABASE_H
#define CTABASE_H

#include <string>

#include "ctainterfacebase.h"

class CTABase
{
private:
    std::string ctaName;

protected:
    CTAInterfaceBase *log;

    virtual std::string GetClassNameInternal();
    void SetName(std::string NewName);

public:
    CTABase(std::string Name = "", CTAInterfaceBase *Log = nullptr);
    std::string GetClassName();
    std::string GetName();
    CTAInterfaceBase *GetLog();
    void LogMessage(eMessageType Type, std::string Message);


};

#endif // CTABASE_H
