#include <list>
#include <algorithm>
#include <QSemaphore>

#include "ctainterfacebase.h"
#include "ctacar.h"
#include "ctapath.h"
#include "ctacorner.h"


int CTAInterfaceBase::getHorizontalCount() const
{
    return horizontalCount;
}

int CTAInterfaceBase::getVerticalCount() const
{
    return verticalCount;
}

int CTAInterfaceBase::getBlockSize() const
{
    return blockSize;
}

int CTAInterfaceBase::getBlockLanes() const
{
    return blockLanes;
}

bool CTAInterfaceBase::MessageTypeActive(eMessageType Type)
{
    return (std::find(activeTypes.begin(), activeTypes.end(), Type) != activeTypes.end());;
}


CTAInterfaceBase::CTAInterfaceBase()
{
    activeTypes = {mtSuccess, mtWarning, mtError, mtDebug};

}

void CTAInterfaceBase::LogMessage(CTABase *Object, eMessageType Type, std::string Message)
{
    if(MessageTypeActive(Type))
        LogMessageInternal(Object, Type, Message);

}

MessageTypeList CTAInterfaceBase::GetActiveTypes()
{
    return activeTypes;

}

void CTAInterfaceBase::EnableType(eMessageType Type)
{
    activeTypes.merge({ Type });
}

void CTAInterfaceBase::DisableType(eMessageType Type)
{
    activeTypes.remove(Type);
}


void CTAInterfaceBase::InitializeMatrix(int HorizontalCount, int VerticalCount, int BlockSize, int BlockLanes)
{
    horizontalCount = HorizontalCount;
    verticalCount = VerticalCount;
    blockSize = BlockSize;
    blockLanes = BlockLanes;

    InitializeMatrixInternal(horizontalCount, verticalCount, blockSize, blockLanes);
}

void CTAInterfaceBase::RefreshMatrix()
{
    RefreshMatrixInternal();
}

void CTAInterfaceBase::SetCarToPath(CTAPath *path, int Hor, int Ver, CTACar *Car)
{
    SetCarToPathInternal(path, Hor, Ver, Car);
}

void CTAInterfaceBase::SetCorner(CTACorner *corner)
{
    SetCornerInternal(corner);
}

