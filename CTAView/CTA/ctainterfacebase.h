#ifndef CTALOGITF_H
#define CTALOGITF_H

#include <string>

#include "messagetype.h"
#include "ctadirection.h"

class CTABase;
class CTACar;
class CTAPath;
class CTACorner;

class CTAInterfaceBase
{
private:
    MessageTypeList activeTypes;
    int horizontalCount;
    int verticalCount;
    int blockSize;
    int blockLanes;

    bool MessageTypeActive(eMessageType Type);

protected:

    virtual void LogMessageInternal(CTABase *Object, eMessageType Type, std::string Message)=0;
    virtual void InitializeMatrixInternal(int HorizontalCount, int VerticalCount, int BlockSize, int BlockLanes)=0;
    virtual void SetCarToPathInternal(CTAPath *path, int Hor, int Ver, CTACar *Car)=0;
    virtual void RefreshMatrixInternal()=0;
    virtual void SetCornerInternal(CTACorner *corner)=0;

public:
    CTAInterfaceBase();

    void LogMessage(CTABase *Object, eMessageType Type, std::string Message);
    MessageTypeList GetActiveTypes();
    void EnableType(eMessageType Type);
    void DisableType(eMessageType Type);
    void InitializeMatrix(int HorizontalCount, int VerticalCount, int BlockSize, int BlockLanes);
    void RefreshMatrix();
    void SetCarToPath(CTAPath *path, int Hor, int Ver, CTACar *Car);
    void SetCorner(CTACorner *corner);

    int getHorizontalCount() const;
    int getVerticalCount() const;
    int getBlockSize() const;
    int getBlockLanes() const;
};

typedef CTAInterfaceBase *PCTAInterfaceBase;

#endif // CTALOGITF_H
