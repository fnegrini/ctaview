#include "ctapath.h"
#include "ctacar.h"


int CTAPath::getHorCount() const
{
    return horCount;
}

int CTAPath::getVerCount() const
{
    return verCount;
}

int CTAPath::getHorOffSet()
{
    return getHorOffSetInternal();
}

int CTAPath::getVerOffSet()
{
    return getVerOffSetInternal();
}

CTAPath *CTAPath::getPathUp() const
{
    return pathUp;
}

void CTAPath::setPathUp(CTAPath *value)
{
    pathUp = value;
}

CTAPath *CTAPath::getPathDown() const
{
    return pathDown;
}

void CTAPath::setPathDown(CTAPath *value)
{
    pathDown = value;
}

CTAPath *CTAPath::getPathLeft() const
{
    return pathLeft;
}

void CTAPath::setPathLeft(CTAPath *value)
{
    pathLeft = value;
}

CTAPath *CTAPath::getPathRight() const
{
    return pathRight;
}

void CTAPath::setPathRight(CTAPath *value)
{
    pathRight = value;
}

eDirection CTAPath::getHorDirection() const
{
    return horDirection;
}

void CTAPath::setHorDirection(const eDirection &value)
{
    horDirection = value;
}

eDirection CTAPath::getVerDirection() const
{
    return verDirection;
}

void CTAPath::setVerDirection(const eDirection &value)
{
    verDirection = value;
}

ePathStatus CTAPath::getHorStatus() const
{
    return horStatus;
}

void CTAPath::setHorStatus(const ePathStatus &value)
{
    horStatus = value;
}

ePathStatus CTAPath::getVerStatus() const
{
    return verStatus;
}

void CTAPath::setVerStatus(const ePathStatus &value)
{
    verStatus = value;
}

CTAPathFull *CTAPath::GetNextPath(int HorPos, int VerPos, eCarDirectioon Direction)
{
    PCTAPath path = nullptr;
    int NewHorPos = -1;
    int NewVerPos = -1;

    switch (Direction) {

    case cdBottomUp:
        if(VerPos==0){
            if(pathUp!=nullptr){
                path = pathUp;
                NewHorPos = HorPos;
                NewVerPos = path->getVerCount()-1; // Last position
            }
        }else{
            NewVerPos = VerPos-1;
            NewHorPos = HorPos;
            path = this;
        }
        break;

    case cdRightLeft:
        if(HorPos==0){
            if(pathLeft!=nullptr){
                path = pathLeft;
                NewHorPos = path->getHorCount()-1; // Last position
                NewVerPos = VerPos;
            }
        }else{
            NewVerPos = VerPos;
            NewHorPos = HorPos-1;
            path = this;
        }
        break;

    case cdUpDown:
        if(VerPos==(verCount-1)){
            if(pathDown!=nullptr){
                path = pathDown;
                NewHorPos = HorPos;
                NewVerPos = 0; // First position
            }
        }else{
            NewVerPos = VerPos+1;
            NewHorPos = HorPos;
            path = this;
        }

        break;

    case cdLeftRight:
        if(HorPos==(horCount-1)){
            if(pathRight!=nullptr){
                path = pathRight;
                NewHorPos = 0; // First Position
                NewVerPos = VerPos;
            }
        }else{
            NewVerPos = VerPos;
            NewHorPos = HorPos+1;
            path = this;
        }
        break;

    }

    return new CTAPathFull(path, NewHorPos, NewVerPos);

}

CTAPathFull *CTAPath::GetFreeFirstPos(eCarDirectioon Direction)
{
    int HorPos = -1;
    int VerPos = -1;

    switch (Direction) {

    case cdBottomUp:
        if(revertNewLane){
            for (int i = 0; i < horCount; ++i) {
                if(GetPosition(i,verCount-1)==nullptr){
                    HorPos = i;
                    VerPos = verCount-1;
                    break;
                }
            }
        }else{
            for (int i = horCount-1; i >= horCount; --i) {
                if(GetPosition(i,verCount-1)==nullptr){
                    HorPos = i;
                    VerPos = verCount-1;
                    break;
                }
            }

        }
        break;

    case cdRightLeft:
        if(revertNewLane){
            for (int i = 0; i < verCount; ++i) {
                if(GetPosition(horCount-1,i)==nullptr){
                    HorPos = horCount-1;
                    VerPos = i;
                    break;
                }
            }
        }else{
            for (int i = verCount-1; i >= 0 ; --i) {
                if(GetPosition(horCount-1,i)==nullptr){
                    HorPos = horCount-1;
                    VerPos = i;
                    break;
                }
            }

        }
        break;

    case cdUpDown:
        if(revertNewLane){
            for (int i = 0; i < horCount; ++i) {
                if(GetPosition(i,0)==nullptr){
                    HorPos = i;
                    VerPos = 0;
                    break;
                }
            }
        }else{
            for (int i = horCount-1; i >= 0; --i) {
                if(GetPosition(i,0)==nullptr){
                    HorPos = i;
                    VerPos = 0;
                    break;
                }
            }
        }
        break;

    case cdLeftRight:
        if(revertNewLane){
            for (int i = 0; i < verCount; ++i) {
                if(GetPosition(0,i)==nullptr){
                    HorPos = 0;
                    VerPos = i;
                    break;
                }
            }
        }else{
            for (int i = verCount-1; i >= 0 ; --i) {
                if(GetPosition(0,i)==nullptr){
                    HorPos = 0;
                    VerPos = i;
                    break;
                }
            }

        }
        break;

    }

    revertNewLane = (!revertNewLane);

    return new CTAPathFull(this, HorPos, VerPos);
}

CTAPath::CTAPath(std::string Name, CTAInterfaceBase *Log)
    :CTABase(Name,Log)
{

    pathUp    = nullptr;
    pathDown  = nullptr;
    pathLeft  = nullptr;
    pathRight = nullptr;

    horDirection = gdNone;
    verDirection = gdNone;

    horStatus = psActive;
    verStatus = psActive;

    horCount = 0;
    verCount = 0;

    revertNewLane = false;
}

void CTAPath::InitializePath(int HorCount, int VerCount)
{
    matrix = new CTACar**[HorCount];

    horCount = HorCount;
    verCount = VerCount;

    for (int iHor = 0; iHor < HorCount; ++iHor) {
        matrix[iHor] = new CTACar*[VerCount];
        for (int iVer = 0; iVer < VerCount; ++iVer) {
            matrix[iHor][iVer] = nullptr;
        }
    }

}

CTACar *CTAPath::GetPosition(int HorPos, int VerPos)
{
    if (CheckPosition(HorPos, VerPos))
        return matrix[HorPos][VerPos];
    else
        return nullptr;
}

bool CTAPath::CheckPosition(int HorPos, int VerPos)
{
    return ((HorPos < horCount)&&(VerPos < verCount));

}

bool CTAPath::SetPosition(CTACar *Car, int HorPos, int VerPos)
{
    if(CheckPosition(HorPos, VerPos)){
        matrix[HorPos][VerPos] = Car;
        if (log != nullptr)
            log->SetCarToPath(this, HorPos, VerPos, Car);
        return true;
    }else{
        return false;
    }
}

CTAPath *CTAPathFull::getPath() const
{
    return path;
}

int CTAPathFull::getHorPos() const
{
    return horPos;
}

int CTAPathFull::getVerPos() const
{
    return verPos;
}

CTAPathFull::CTAPathFull(CTAPath *Path, int HorPos, int VerPos)
{
    path = Path;
    horPos = HorPos;
    verPos = VerPos;
}
