#ifndef CTABLOCK_H
#define CTABLOCK_H

#include <vector>

#include "ctapath.h"
#include "ctaorientation.h"
#include "ctacorner.h"
#include "ctaconstants.h"
#include "ctapath.h"
#include "ctadirection.h"

class CTAStreet;
class CTACar;

class CTABlock : public CTAPath
{
private:
    CTAStreet *street;
    int blockCount;

protected:
    virtual int getHorOffSetInternal();
    virtual int getVerOffSetInternal();
    virtual std::string GetClassNameInternal() override;
public:
    CTABlock(std::string Name = "", CTAInterfaceBase *Log = nullptr, int BlockCount = 0, CTAStreet *Street = nullptr);
    ~CTABlock();
    void SetCorner(CTACorner *Corner);
    eOrientation GetOrientation();
    eDirection GetDirection();


};

typedef CTABlock *PCTABlock;

typedef std::vector<PCTABlock> CTABLockList;

#endif // CTABLOCK_H
