#ifndef CTACORNER_H
#define CTACORNER_H

#include <vector>

#include "ctabase.h"
#include "ctapath.h"
#include "ctasemaphore.h"
#include "ctaconstants.h"

class CTABlock;

class CTACorner : public CTAPath
{
private:

    eSemaphoreState semaphore;
    int horOffSet;
    int verOffSet;

protected:
    virtual int getHorOffSetInternal() override;
    virtual int getVerOffSetInternal() override;
    virtual std::string GetClassNameInternal() override;

public:
    CTACorner(std::string Name = "", CTAInterfaceBase *Log = nullptr, int HorOffSet = 0, int VerOffSet = 0);
    ~CTACorner();
    bool NextSemaphoreStateValid(eSemaphoreState Next);

    eSemaphoreState getSemaphore() const;
    bool setSemaphore(const eSemaphoreState value);
};

typedef CTACorner *PCTACorner;


#endif // CTACORNER_H
