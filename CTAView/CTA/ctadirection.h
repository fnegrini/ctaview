#ifndef CTADIRECTION_H
#define CTADIRECTION_H

enum eDirection {
    gdNone = -1,
    gdBeginToEnd = 0,
    gdEndToBegin = 1,
};

enum eStreetDirection {
    sdHorizontal = 0,
    sdVertical = 1
};

enum eCarDirectioon {
    cdUpDown = 0,
    cdBottomUp = 1,
    cdLeftRight = 2,
    cdRightLeft = 3,
};

#endif // CTADIRECTION_H
