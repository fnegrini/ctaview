#include "ctaeventhandlerbase.h"
#include "ctasimulator.h"

CTAEventHandlerBase::CTAEventHandlerBase()
{

}

CTAInitValues *CTAEventHandlerBase::getInitValues()
{
    return getInitValuesInternal();
}

void CTAEventHandlerBase::HandleEvents(CTASimulator *simulator)
{
    HandleEventsInternal(simulator);
}



int CTAInitValues::getHorizontalCount() const
{
    return horizontalCount;
}

int CTAInitValues::getVerticalCount() const
{
    return verticalCount;
}

eSimulationType CTAInitValues::getSimulationType() const
{
    return simulationType;
}

eDirection CTAInitValues::getHDirectionFirstStreet() const
{
    return hDirectionFirstStreet;
}

eDirection CTAInitValues::getVDirectionFirstStreet() const
{
    return vDirectionFirstStreet;
}

unsigned long CTAInitValues::getInterval() const
{
    return interval;
}

int CTAInitValues::getBlockSize() const
{
    return blockSize;
}

int CTAInitValues::getBlockLanes() const
{
    return blockLanes;
}

unsigned int CTAInitValues::getCarSpeed() const
{
    return carSpeed;
}

CTAInitValues::CTAInitValues(int HorizontalCount, int VerticalCount,
                             int BlockSize, int BlockLanes,
                             eSimulationType SimulationType,
                             eDirection HDirectionFirstStreet, eDirection VDirectionFirstStreet,
                             unsigned long Interval, unsigned int CarSpeed)
{
    horizontalCount = HorizontalCount;
    verticalCount = VerticalCount;
    blockSize = BlockSize;
    blockLanes = BlockLanes;
    simulationType = SimulationType;
    hDirectionFirstStreet = HDirectionFirstStreet;
    vDirectionFirstStreet = VDirectionFirstStreet;
    interval = Interval;
    carSpeed = CarSpeed;
}


