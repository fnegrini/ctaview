#ifndef CTACARBUFFER_H
#define CTACARBUFFER_H

#include <list>

#include "ctadirection.h"

class CTACarBuffer
{
private:
    int carID;
    eStreetDirection streetDirection;
    int street;
public:
    CTACarBuffer(int CarID, eStreetDirection StreetDirection, int Street);
    int getCarID() const;
    eStreetDirection getStreetDirection() const;
    int getStreet() const;
};

typedef CTACarBuffer *PCTACarBuffer;
typedef std::list<PCTACarBuffer> CTACarBufferList;
typedef std::list<PCTACarBuffer>::iterator CTACarBufferIterator;

#endif // CTACARBUFFER_H
