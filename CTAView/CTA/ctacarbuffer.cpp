#include "ctacarbuffer.h"


int CTACarBuffer::getCarID() const
{
    return carID;
}

eStreetDirection CTACarBuffer::getStreetDirection() const
{
    return streetDirection;
}

int CTACarBuffer::getStreet() const
{
    return street;
}


CTACarBuffer::CTACarBuffer(int CarID, eStreetDirection StreetDirection, int Street)
{
    carID = CarID;
    streetDirection = StreetDirection;
    street = Street;
}
