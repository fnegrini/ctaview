#include "ctacar.h"
#include "ctablock.h"

int CTACar::getCarID() const
{
    return carID;
}

void CTACar::setCarID(int value)
{
    carID = value;
}


CTAPath *CTACar::getPath() const
{
    return path;
}

void CTACar::setPath(CTAPath *value, int HorPos, int VerPos)
{
    // Clear the actual posticion
    if(path!=nullptr)
        path->SetPosition(nullptr, horPos, verPos);

    // Assign values
    path = value;
    horPos = HorPos;
    verPos = VerPos;

    // Set the car in the position
    if(path != nullptr)
        path->SetPosition(this, horPos, verPos);

}

eCarDirectioon CTACar::getDirection() const
{
    return direction;
}

void CTACar::setDirection(const eCarDirectioon &value)
{
    direction = value;
}

bool CTACar::Move(unsigned int CarSpeed, bool Turn, bool LogErrors)
{
    bool result = true, resultAux;

    for (int i = 0; i < CarSpeed; ++i) {
        resultAux = MoveStep(Turn, LogErrors);
        if(!resultAux)
            result = resultAux;
    }

    return result;
}

bool CTACar::MoveStep(bool Turn, bool LogErrors)
{
    std::string message;
    PCTAPathFull pathFull;
    PCTACar car;
    bool directionOK;
    bool statusOK;
    bool result = true;

    // Get Actual Path
    if (path == nullptr){
        if(LogErrors){
            message = "Car " + std::to_string(carID) + " not in a valid Path";
            LogMessage(mtError, message);
        }
        return false;
    }

    if(Turn){
        if((direction==cdUpDown)||(direction==cdBottomUp)){
            switch(path->getHorDirection()){
            /*case gdNone:
                if(LogErrors){
                    message = "Car " + std::to_string(carID) + " cant turn in this way";
                    LogMessage(mtError, message);
                }
                return false;
                break;*/
            case gdBeginToEnd:
                setDirection(cdLeftRight);
                  break;
            case gdEndToBegin:
                setDirection(cdRightLeft);
                break;
            }
        }else{
            switch(path->getVerDirection()){
            /*case gdNone:
                if(LogErrors){
                    message = "Car " + std::to_string(carID) + " cant turn in this way";
                    LogMessage(mtError, message);
                }
                return false;
                break;*/
            case gdBeginToEnd:
                setDirection(cdUpDown);
                break;

            case gdEndToBegin:
                setDirection(cdBottomUp);
                break;
            }

        }
    }

    // Ask path next position (maybe a new path)
    pathFull = path->GetNextPath(horPos, verPos, direction);
    try {

        // Next path empty ==> end of path
        if (pathFull->getPath()==nullptr){
            message = "Car " + std::to_string(carID) + " leaves de simulation";
            LogMessage(mtSuccess, message);
            setPath(pathFull->getPath(), pathFull->getHorPos(), pathFull->getVerPos());

        }else{

            // Check if the next position is empty
            car = pathFull->getPath()->GetPosition(pathFull->getHorPos(), pathFull->getVerPos());
            if (car!=nullptr){
                if(LogErrors){
                    message = "Car " + std::to_string(carID) + " crashed with car " + std::to_string(car->getCarID());
                    LogMessage(mtError, message);
                }
                result = false;
            }

            // Check if is in the right direction for path
            if (result){
                switch (direction) {

                case cdUpDown:
                    directionOK = (pathFull->getPath()->getVerDirection()==gdBeginToEnd);
                    break;
                case cdBottomUp:
                    directionOK = (pathFull->getPath()->getVerDirection()==gdEndToBegin);
                    break;
                case cdLeftRight:
                    directionOK = (pathFull->getPath()->getHorDirection()==gdBeginToEnd);
                    break;
                case cdRightLeft:
                    directionOK = (pathFull->getPath()->getHorDirection()==gdEndToBegin);
                    break;
                }

                if(!directionOK){
                    if(LogErrors){
                        message = "Car " + std::to_string(carID) + " is in a invalid direction";
                        LogMessage(mtError, message);
                    }
                    result = false;
                }
            }

            if((result)&&(path!=pathFull->getPath())){
                // Check if the next position is direction active
                switch (direction) {

                case cdUpDown:
                    statusOK = (pathFull->getPath()->getVerStatus()!=psBlocked);
                    break;
                case cdBottomUp:
                    statusOK = (pathFull->getPath()->getVerStatus()!=psBlocked);
                    break;
                case cdLeftRight:
                    statusOK = (pathFull->getPath()->getHorStatus()!=psBlocked);
                    break;
                case cdRightLeft:
                    statusOK = (pathFull->getPath()->getHorStatus()!=psBlocked);
                    break;
                }
                if(!statusOK){
                    if(LogErrors){
                        message = "Car " + std::to_string(carID) + " can't move. Direction blocked";
                        LogMessage(mtError, message);
                    }
                    result = false;
                }
            }
            if(result){
                // Set next position
                setPath(pathFull->getPath(), pathFull->getHorPos(), pathFull->getVerPos());
            }
        }

    } catch (const std::exception&) {

        delete pathFull;
    }

    return result;
}

std::string CTACar::GetClassNameInternal()
{
    return "Car";
}

CTACar::CTACar(int CarID, eCarDirectioon Direction)
{
    carID = CarID;
    direction = Direction;
    path = nullptr;

}

CTACar::~CTACar()
{
    // Removes car from path before leave
    setPath(nullptr, 0, 0);
}
