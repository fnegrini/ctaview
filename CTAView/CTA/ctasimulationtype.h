#ifndef CTASIMULATIONTYPE_H
#define CTASIMULATIONTYPE_H

enum eSimulationType {
    stSimplified = 0,
    stComplete = 1,
};

#endif // CTASIMULATIONTYPE_H
