#include "ctastreet.h"

CTABLockList CTAStreet::getBlockList() const
{
    return blockList;
}

PCTABlock CTAStreet::GetFirstBlock()
{
    switch (direction) {
    case gdBeginToEnd:
        return blockList[0];

    case gdEndToBegin:
        return blockList[blockCount-1];

    default:
        return nullptr;
    }
}

int CTAStreet::GetFirstPosition()
{
    switch (direction) {
    case gdBeginToEnd:
        return 0;

    case gdEndToBegin:
        return log->getBlockSize() - 1;

    default:
        return -1;
    }
}

int CTAStreet::GetLastPosition()
{
    switch (direction) {
    case gdBeginToEnd:
        return log->getBlockSize() - 1;

    case gdEndToBegin:
        return 0;

    default:
        return -1;
    }
}

int CTAStreet::getStreetCount() const
{
    return streetCount;
}

void CTAStreet::CreateBlocks(int BlockCount)
{
    std::string name;
    CTABlock *item;

    blockCount = BlockCount;

    for (int i = 0; i < BlockCount; ++i) {
        name = GetName() + "." + std::to_string(i);
        item = new CTABlock(name, GetLog(), i, this);
        blockList.push_back(item);

    }
}

std::string CTAStreet::GetClassNameInternal()
{
    return "Street";
}

CTAStreet::CTAStreet(std::string Name, CTAInterfaceBase *Log,
                     eOrientation Orientation, eDirection Direction,
                     int StreetCount, int BlockCount)
    :CTABase(Name,Log)
{
    orientation = Orientation;
    direction = Direction;
    streetCount = StreetCount;

    CreateBlocks(BlockCount);
}

CTAStreet::~CTAStreet()
{
    PCTABlock block;

    while (blockList.size() > 0) {
        block = blockList.back();
        blockList.pop_back();
        delete block;
    }
}

eOrientation CTAStreet::GetOrientation()
{
    return orientation;
}

eDirection CTAStreet::GetDirection()
{
    return direction;
}
