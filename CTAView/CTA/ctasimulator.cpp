#include "ctasimulator.h"


bool CTASimulator::InitializeStreets()
{
    bool result;
    
    result = InitializeHStreets();

    if(result)
        result = InitializeVStreets();

    return result;
}

bool CTASimulator::InitializeHStreets()
{
    std::string message;
    eDirection directionAux;
    CTAStreet *item;

    if(horizontalCount>MAX_STREET_COUNT){
        message = "Horizontal street count exceeds max street count. " + std::to_string(horizontalCount);
        LogMessage(mtError, message);
        return false;
    }

    directionAux = hDirectionFirstStreet;

    for (int i = 0; i < horizontalCount; ++i) {
        item = new CTAStreet(std::to_string(i), GetLog(), oHorizontal, directionAux, i, verticalCount+1);
        hStreetList.push_back(item);

        if(directionAux==gdBeginToEnd)
            directionAux = gdEndToBegin;
        else
            directionAux = gdBeginToEnd;
    }
    return true;
}

bool CTASimulator::InitializeVStreets()
{

    std::string message;
    eDirection directionAux;
    CTAStreet *item;

    if(verticalCount>MAX_STREET_COUNT){
        message = "Vertical street count exceeds max street count. " + std::to_string(verticalCount);
        LogMessage(mtError, message);
        return false;
    }

    for (int i = 0; i < verticalCount; ++i) {
        item = new CTAStreet(std::to_string(i), GetLog(), oVertical, directionAux, i, horizontalCount+1);
        vStreetList.push_back(item);

        if(directionAux==gdBeginToEnd)
            directionAux = gdEndToBegin;
        else
            directionAux = gdBeginToEnd;
    }

    return true;
}

bool CTASimulator::InitializeCorners()
{
    CTACorner *item;
    std::string name;

    for (int iHor = 0; iHor < horizontalCount; ++iHor) {

        for (int iVer = 0; iVer < verticalCount; ++iVer) {
            name = std::to_string(iHor) + "." + std::to_string(iVer);
            item = new CTACorner(name, GetLog(), iHor, iVer);
            cornerMatrix[iHor][iVer] = item;

        }

    }

    return true;
}

bool CTASimulator::MergeBlocksAndCorners()
{
    CTACorner *item;

    for (int iHor = 0; iHor < horizontalCount; ++iHor) {
        for (int iVer = 0; iVer < verticalCount; ++iVer) {
            item = cornerMatrix[iHor][iVer];

            item->setPathLeft(hStreetList[iVer]->getBlockList()[iHor]);
            item->setHorDirection(hStreetList[iVer]->GetDirection());
            hStreetList[iVer]->getBlockList()[iHor]->setPathRight(item);

            item->setPathUp(vStreetList[iHor]->getBlockList()[iVer]);
            item->setVerDirection(vStreetList[iHor]->GetDirection());
            vStreetList[iHor]->getBlockList()[iVer]->setPathDown(item);


            if( hStreetList[iVer]->getBlockList()[iHor+1] != nullptr){
                item->setPathRight(hStreetList[iVer]->getBlockList()[iHor+1]);
                hStreetList[iVer]->getBlockList()[iHor+1]->setPathLeft(item);
            }

            if( vStreetList[iHor]->getBlockList()[iVer+1] != nullptr){
                item->setPathDown(vStreetList[iHor]->getBlockList()[iVer+1]);
                vStreetList[iHor]->getBlockList()[iVer+1]->setPathUp(item);
            }

            item->setSemaphore(ssNone);

            if(log!=nullptr){
                log->SetCorner(item);
            }

        }
    }
    return true;
}

PCTACar CTASimulator::CarByID(int CarID)
{
    CTACarIterator iterator;
    PCTACar item;

    for (iterator = carList.begin(); iterator != carList.end(); ++iterator) {
        item = *iterator;
        if(item->getCarID() == CarID)
            return item;

    }

    return nullptr;
}

void CTASimulator::MoveAllCars()
{

    CTACarIterator iterator;
    PCTACar item;

    for (iterator = carList.begin(); iterator != carList.end(); ++iterator) {

        item = *iterator;

        item->Move(carSpeed, false, false);
    }

}

void CTASimulator::CheckBuffer()
{
    CTACarBufferIterator iterator;
    PCTACarBuffer item;

    iterator = carBufferList.begin();

    while(iterator != carBufferList.end()){
        item = *iterator;
        // Try to add car to buffer
        if(CheckCarBuffer(item->getCarID(), item->getStreetDirection(), item->getStreet())){
            carBufferList.erase(iterator++);
            delete item;
        }else{
            ++iterator;
        }
    }
}

void CTASimulator::IntializeCornerMatrix()
{
    for (int iHor = 0; iHor < MAX_STREET_COUNT; ++iHor) {
        for (int iVer = 0; iVer < MAX_STREET_COUNT; ++iVer) {
            cornerMatrix[iHor][iVer] = nullptr;
        }
    }
}


void CTASimulator::InitializeGraphMatrix()
{

    if(GetLog() != nullptr){
        GetLog()->InitializeMatrix(horizontalCount, verticalCount, blockSize, blockLanes);
    }

}


std::string CTASimulator::GetClassNameInternal()
{
    return "Simulator";
}

void CTASimulator::run()
{

    LogMessage(mtSuccess, "Simulation started");

    eventHandler->HandleEvents(this);

    LogMessage(mtSuccess, "Simulation finished");

    quit();

}

CTASimulator::CTASimulator(std::string Name, CTAInterfaceBase *Log, CTAEventHandlerBase *EventHandler)
    :CTABase(Name, Log)
{
    horizontalCount = 0;
    verticalCount = 0;
    simulationType = stSimplified;
    hDirectionFirstStreet = gdBeginToEnd;
    vDirectionFirstStreet = gdBeginToEnd;
    interval = 0;
    eventHandler = EventHandler;

}

CTASimulator::~CTASimulator()
{
    PCTACar car;
    PCTAStreet street;

    // Destroy cars
    while (carList.size() > 0) {
        car = carList.back();
        carList.pop_back();
        delete car;
    }

    // Destroy Corners
    for (int iHor = 0; iHor < MAX_STREET_COUNT; ++iHor) {
        for (int iVer = 0; iVer < MAX_STREET_COUNT; ++iVer) {
            if (cornerMatrix[iHor][iVer] != nullptr){
                delete cornerMatrix[iHor][iVer];
                cornerMatrix[iHor][iVer] = nullptr;
            }
        }
    }

    // Destroy Horizontal streets
    while (hStreetList.size() > 0) {
        street = hStreetList.back();
        hStreetList.pop_back();
        delete street;
    }

    // Destroy Vertical streets
    while (vStreetList.size() > 0) {
        street = vStreetList.back();
        vStreetList.pop_back();
        delete street;
    }



}

bool CTASimulator::Initialize(int HorizontalCount, int VerticalCount,
                              int BlockSize, int BlockLanes,
                              eSimulationType SimulationType,
                              eDirection HDirectionFirstStreet, eDirection VDirectionFirstStreet,
                              unsigned long Interval, unsigned int CarSpeed)
{
    horizontalCount = HorizontalCount;
    verticalCount = VerticalCount;
    blockSize = BlockSize;
    blockLanes = BlockLanes;
    simulationType = SimulationType;
    hDirectionFirstStreet = HDirectionFirstStreet;
    vDirectionFirstStreet = VDirectionFirstStreet;
    interval = Interval;
    carSpeed = CarSpeed;

    IntializeCornerMatrix();
    InitializeGraphMatrix();
    InitializeObjects();
}


bool CTASimulator::InitializeObjects()
{
    bool result;
    
    result = InitializeStreets();

    if(result)
        result = InitializeCorners();

    if(result)
        result = MergeBlocksAndCorners();

    return result;

}

bool CTASimulator::HandleEventCarNew(int CarID, eStreetDirection StreetDirection, int Street)
{
    std::string message;
    PCTACar car;

    // Check if ID already exists
    car = CarByID(CarID);

    if(car!=nullptr){
        message = "Car ID already exists: " + std::to_string(CarID);
        LogMessage(mtError, message);
        return false;
    }

    // Check Street exists
    switch (StreetDirection) {
    case sdHorizontal:
        if(Street >= horizontalCount){
            message = "Horizontal Street out of range: " + std::to_string(Street);
            LogMessage(mtError, message);
            return false;
        }

        break;

    case sdVertical:
        if(Street >= verticalCount){
            message = "Vertical Street out of range: " + std::to_string(Street);
            LogMessage(mtError, message);
            return false;
        }

        break;

    default:

        return false;
    }

    carBufferList.push_back(new CTACarBuffer(CarID, StreetDirection, Street));

    if(log != nullptr)
        log->RefreshMatrix();

    return true;
}

bool CTASimulator::CheckCarBuffer(int CarID, eStreetDirection StreetDirection, int Street)
{
    std::string message;
    PCTACar car;
    eCarDirectioon direction;
    PCTABlock block;
    PCTAPathFull pathFull;

    // Check Street exists
    switch (StreetDirection) {
    case sdHorizontal:
        block = hStreetList[Street]->GetFirstBlock();

        switch (hStreetList[Street]->GetDirection()) {

        case gdBeginToEnd:
            direction = cdLeftRight;
            break;
        case gdEndToBegin:
            direction = cdRightLeft;
            break;
        default:
            direction = cdLeftRight;
            break;
        }

        break;

    case sdVertical:
        if(Street >= verticalCount){
            message = "Vertical Street out of range: " + std::to_string(Street);
            LogMessage(mtError, message);
            return false;
        }

        block = vStreetList[Street]->GetFirstBlock();

        switch (vStreetList[Street]->GetDirection()) {

        case gdBeginToEnd:
            direction = cdUpDown;
            break;
        case gdEndToBegin:
            direction = cdBottomUp;
            break;
        default:
            direction = cdUpDown;
            break;
        }

        break;

    default:

        return false;
    }

    // Check Street has free space
    pathFull = block->GetFreeFirstPos(direction);

    if((pathFull->getHorPos()==-1)||(pathFull->getVerPos()==-1)){
        return false;
    }

    car = new CTACar(CarID, direction);
    car->setPath(block, pathFull->getHorPos(), pathFull->getVerPos());
    message = "Car ID " + std::to_string(CarID) + " sucessfuly added to block " + block->GetName();
    LogMessage(mtSuccess, message);
    carList.push_back(car);
    delete pathFull;

    return true;

}

bool CTASimulator::HandleEventCarMove(int CarID, bool Convert, unsigned int count)
{
    PCTACar car;
    std::string message;
    bool result;

    car = CarByID(CarID);

    if(car==nullptr){
        message = "Car " + std::to_string(CarID) + " does not exist";
        LogMessage(mtError, message);
        return false;

    }

    for (int i = 0; i < count; ++i) {

        result = car->Move(carSpeed, Convert, false);
    }

    if(log != nullptr)
        log->RefreshMatrix();

    return result;

}

bool CTASimulator::HandleEventCarExit(int CarID)
{
    PCTACar car;
    std::string message;

    car = CarByID(CarID);

    if(car==nullptr){
        message = "Car " + std::to_string(CarID) + " does not exist";
        LogMessage(mtError, message);
        return false;

    }

    car->setPath(nullptr,0,0);

    if(log != nullptr)
        log->RefreshMatrix();

    return true;

}

bool CTASimulator::HandleEventClock(int ClockCount)
{


    for (int iCount = 0; iCount < ClockCount; ++iCount) {

        if(log != nullptr)
            log->RefreshMatrix();

        msleep(interval);

        if(simulationType == stSimplified){

            MoveAllCars();
        }

        CheckBuffer();

    }

    if(log != nullptr)
        log->RefreshMatrix();

    return true;

}

bool CTASimulator::HandleEventSemaphore(int HorPos, int VerPos, eSemaphoreState State)
{
    CTACorner *corner;
    std::string message;

    if ((HorPos > (horizontalCount-1))||(VerPos > (verticalCount-1))){
            message = "Invalid corner: " + std::to_string(HorPos) + "." + std::to_string(VerPos);
            LogMessage(mtError, message);
            return false;
    }

    corner = cornerMatrix[HorPos][VerPos];
    if(corner==nullptr){
        message = "Corner " + std::to_string(HorPos) + "." + std::to_string(VerPos) +
                " does not exist";
        LogMessage(mtError, message);
        return false;
    }

    if(!corner->NextSemaphoreStateValid(State)){
        message = "Invalid semaphore state for semaphore " + std::to_string(HorPos) + "." + std::to_string(VerPos);
        LogMessage(mtError, message);
        return false;
    }

    corner->setSemaphore(State);

    if(log != nullptr)
        log->RefreshMatrix();

    return true;
}


void CTASimulator::StartSimulation()
{
    CTAInitValues *init;

    if(eventHandler==nullptr){
        LogMessage(mtError, "None Event Handler set");
        return;
     }

    init = eventHandler->getInitValues();

    if (init==nullptr){
        LogMessage(mtError, "Not able to get init parameters from handler");
        return;
    }

    Initialize(init->getHorizontalCount(),
               init->getVerticalCount(),
               init->getBlockSize(),
               init->getBlockLanes(),
               init->getSimulationType(),
               init->getHDirectionFirstStreet(),
               init->getVDirectionFirstStreet(),
               init->getInterval(),
               init->getCarSpeed());

    delete init;

    start();
}

