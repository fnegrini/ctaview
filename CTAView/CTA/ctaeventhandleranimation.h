#ifndef CTAEVENTHANDLERANIMATION_H
#define CTAEVENTHANDLERANIMATION_H

#include <list>

#include "ctaeventhandlerbase.h"

class CTAEventHandlerAnimation : public CTAEventHandlerBase
{
private:

protected:
    virtual CTAInitValues *getInitValuesInternal() override;
    virtual void HandleEventsInternal(CTASimulator *simulator) override;
public:
    CTAEventHandlerAnimation();
};

#endif // CTAEVENTHANDLERANIMATION_H
