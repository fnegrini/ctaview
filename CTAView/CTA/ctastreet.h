#ifndef CTASTREET_H
#define CTASTREET_H

#include <vector>

#include "ctapath.h"
#include "ctaorientation.h"
#include "ctablock.h"
#include "ctadirection.h"

class CTAStreet : public CTABase
{
private:
    eOrientation orientation;
    eDirection direction;
    int streetCount;
    int blockCount;
    CTABLockList blockList;

    void CreateBlocks(int BlockCount);

protected:
    virtual std::string GetClassNameInternal() override;

public:
    CTAStreet(std::string Name = "", CTAInterfaceBase *Log = nullptr,
              eOrientation Orientation = oHorizontal, eDirection Direction = gdBeginToEnd,
              int StreetCount=0, int BlockCount=0);
    ~CTAStreet();
    eOrientation GetOrientation();
    eDirection GetDirection();

    CTABLockList getBlockList() const;
    PCTABlock GetFirstBlock();
    int GetFirstPosition();
    int GetLastPosition();
    int getStreetCount() const;
};

typedef CTAStreet *PCTAStreet;
typedef std::vector<PCTAStreet> CTAStreetList;

#endif // CTASTREET_H
