#include "ctacorner.h"
#include "ctablock.h"


eSemaphoreState CTACorner::getSemaphore() const
{
    return semaphore;
}

bool CTACorner::setSemaphore(const eSemaphoreState value)
{
    if(!NextSemaphoreStateValid(value)){
        LogMessage(mtError, "Invalid sempahore state");
        return false;
    }

    semaphore = value;

    switch (semaphore) {
    case ssNone:
        setHorStatus(psActive);
        setVerStatus(psActive);
        break;

    case ssHorRedVerRedNextHor:
        setHorStatus(psBlocked);
        setVerStatus(psBlocked);
        break;

    case ssHorGreenVerRed:
        setHorStatus(psActive);
        setVerStatus(psBlocked);
        break;

    case ssHorYellowVerRed:
        setHorStatus(psWarning);
        setVerStatus(psBlocked);
        break;

    case ssHorRedVerRedNextVer:
        setHorStatus(psBlocked);
        setVerStatus(psBlocked);
        break;

    case ssHorRedVerGreen:
        setHorStatus(psBlocked);
        setVerStatus(psActive);
        break;

    case ssHorRedVerYellow:
        setHorStatus(psBlocked);
        setVerStatus(psWarning);
        break;

    }

    if(log!=nullptr)
        log->SetCorner(this);

    return true;
}

bool CTACorner::NextSemaphoreStateValid(eSemaphoreState Next)
{
//    if(semaphore==ssNone){
//        return true;
//    }else{
//        // Aceita tanto horizontal quanto vertical verde quando ambos vermelhos
//        if((semaphore==ssHorRedVerRedNextHor)||(semaphore==ssHorRedVerRedNextVer)){
//            return ((Next == ssHorGreenVerRed)||(Next == ssHorRedVerGreen));
//        }else{
//            return (Next == ((semaphore+1) % 6));
//        }
//    }
    return true;
}

int CTACorner::getHorOffSetInternal()
{
    return (horOffSet+1)*log->getBlockSize() + (horOffSet*log->getBlockLanes() );;
}

int CTACorner::getVerOffSetInternal()
{
    return (verOffSet+1)* log->getBlockSize() + (verOffSet*log->getBlockLanes());
}

std::string CTACorner::GetClassNameInternal()
{
    return "Corner";
}

CTACorner::CTACorner(std::string Name, CTAInterfaceBase *Log, int HorOffSet, int VerOffSet)
    :CTAPath(Name, Log)
{
    InitializePath(log->getBlockLanes(), log->getBlockLanes());
    semaphore =  ssNone;
    horOffSet = HorOffSet;
    verOffSet = VerOffSet;
}

CTACorner::~CTACorner()
{

}

