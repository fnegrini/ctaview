#ifndef CTAORIENTATION_H
#define CTAORIENTATION_H

enum eOrientation{
    oNone = -1,
    oHorizontal = 0,
    oVertical = 1,
};

#endif // CTAORIENTATION_H
