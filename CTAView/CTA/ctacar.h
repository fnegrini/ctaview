#ifndef CTACAR_H
#define CTACAR_H

#include "ctabase.h"
#include "ctadirection.h"

#include <list>

class CTAPath;

class CTACar : public CTABase
{
private:
    int carID;
    CTAPath *path;
    int horPos;
    int verPos;
    eCarDirectioon direction;

protected:
    virtual std::string GetClassNameInternal() override;
public:
    CTACar(int CarID, eCarDirectioon Direction);
    ~CTACar();

    int getCarID() const;
    void setCarID(int value);

    CTAPath *getPath() const;
    void setPath(CTAPath *value, int HorPos, int VerPos);

    eCarDirectioon getDirection() const;
    void setDirection(const eCarDirectioon &value);

    bool Move(unsigned int CarSpeed, bool Turn = false, bool LogErrors = true);
    bool MoveStep( bool Turn = false, bool LogErrors = true);

};

typedef CTACar *PCTACar;
typedef std::list<PCTACar> CTACarList;
typedef std::list<PCTACar>::iterator CTACarIterator;
#endif // CTACAR_H
