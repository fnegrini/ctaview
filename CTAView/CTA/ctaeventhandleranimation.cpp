#include "ctaeventhandleranimation.h"
#include "ctasimulator.h"


CTAInitValues *CTAEventHandlerAnimation::getInitValuesInternal()
{
    return new CTAInitValues(2,2,20,2,stSimplified,gdBeginToEnd, gdBeginToEnd, 250, 4);
}

void CTAEventHandlerAnimation::HandleEventsInternal(CTASimulator *simulator)
{
    if(simulator==nullptr) return;

    simulator->HandleEventSemaphore(0,0, ssHorGreenVerRed);
    simulator->HandleEventSemaphore(0,1, ssHorGreenVerRed);
    simulator->HandleEventSemaphore(1,0, ssHorGreenVerRed);
    simulator->HandleEventSemaphore(1,1, ssHorGreenVerRed);

    simulator->HandleEventCarNew(0, sdHorizontal, 0);
    simulator->HandleEventCarNew(1, sdHorizontal, 1);
    simulator->HandleEventClock(20);

}


CTAEventHandlerAnimation::CTAEventHandlerAnimation()
    :CTAEventHandlerBase()
{

}
