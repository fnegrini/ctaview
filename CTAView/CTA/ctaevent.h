#ifndef CTAEVENT_H
#define CTAEVENT_H

enum eEvent {
    evtCarNew = 0,
    evtCarMove = 1,
    evtCarExit = 2,
    evtClock = 3,
    evtSemaphore = 4,
    evtLogMessage = 5,
};

#endif // CTAEVENT_H
