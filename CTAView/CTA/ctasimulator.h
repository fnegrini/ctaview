#ifndef CTASIMULATOR_H
#define CTASIMULATOR_H


#include <list>
#include <QThread>

#include "ctabase.h"
#include "ctadirection.h"
#include "ctaevent.h"
#include "ctastreet.h"
#include "ctacorner.h"
#include "ctaconstants.h"
#include "ctacar.h"
#include "ctaeventhandlerbase.h"
#include "ctasimulationtype.h"
#include "ctacarbuffer.h"

class CTASimulator : public CTABase, private QThread
{
private:
    int horizontalCount;
    int verticalCount;
    int blockSize;
    int blockLanes;
    unsigned int carSpeed;

    eDirection hDirectionFirstStreet;
    eDirection vDirectionFirstStreet;

    CTAStreetList hStreetList;
    CTAStreetList vStreetList;

    eSimulationType simulationType;

    PCTACorner cornerMatrix[MAX_STREET_COUNT][MAX_STREET_COUNT];

    CTACarList carList;
    CTACarBufferList carBufferList;

    CTAEventHandlerBase *eventHandler;

    unsigned long interval;

    bool InitializeObjects();
    bool InitializeStreets();
    bool InitializeHStreets();
    bool InitializeVStreets();
    bool InitializeCorners();
    bool MergeBlocksAndCorners();
    void IntializeCornerMatrix();
    void InitializeGraphMatrix();

    PCTACar CarByID(int CarID);
    void MoveAllCars();
    void CheckBuffer();
    bool CheckCarBuffer(int CarID, eStreetDirection StreetDirection, int Street);

protected:
    virtual std::string GetClassNameInternal() override;
    virtual void run() override;

public:

    CTASimulator(std::string Name = "", CTAInterfaceBase *Log = nullptr, CTAEventHandlerBase *EventHandler = nullptr);
    ~CTASimulator();

    bool Initialize(int HorizontalCount = 10, int VerticalCount = 10,
                    int BlockSize = 5, int BlockLanes = 2,
                    eSimulationType SimulationType = stSimplified,
                    eDirection HDirectionFirstStreet = gdBeginToEnd,
                    eDirection VDirectionFirstStreet = gdBeginToEnd,
                    unsigned long Interval = 1000, unsigned int CarSpeed = 1);


    bool HandleEventCarNew(int CarID, eStreetDirection StreetDirection, int Street);
    bool HandleEventCarMove(int CarID, bool Convert, unsigned int count = 1);
    bool HandleEventCarExit(int CarID);
    bool HandleEventClock(int ClockCount = 1);
    bool HandleEventSemaphore(int HorPos, int VerPos, eSemaphoreState State);
    void StartSimulation();

};

#endif // CTASIMULATOR_H
