#ifndef MESSAGETYPE_H
#define MESSAGETYPE_H

#include <list>

enum eMessageType {
    mtSuccess = 0,
    mtWarning = 1,
    mtError = 2,
    mtDebug = 3,
};

typedef std::list<eMessageType> MessageTypeList;

#endif // MESSAGETYPE_H
