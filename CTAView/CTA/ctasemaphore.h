#ifndef CTASEMAPHORE_H
#define CTASEMAPHORE_H

enum eSemaphoreState {
    ssNone = -1,
    ssHorRedVerRedNextHor = 0,
    ssHorGreenVerRed = 1,
    ssHorYellowVerRed = 2,
    ssHorRedVerRedNextVer = 3,
    ssHorRedVerGreen = 4,
    ssHorRedVerYellow = 5,
};

#endif // CTASEMAPHORE_H
