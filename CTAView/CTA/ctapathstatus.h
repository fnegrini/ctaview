#ifndef PATHSTATUS_H
#define PATHSTATUS_H

enum ePathStatus {

    psActive = 0,
    psWarning = 1,
    psBlocked = 2,

};

#endif // PATHSTATUS_H
