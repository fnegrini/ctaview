#include "ctabase.h"

CTABase::CTABase(std::string Name, CTAInterfaceBase *Log)
{
    SetName(Name);
    log = Log;
}

std::string CTABase::GetClassNameInternal()
{
    return "CTABase";
}

void CTABase::SetName(std::string NewName)
{
    ctaName = NewName;
}

std::string CTABase::GetClassName()
{
    return GetClassNameInternal();
}

std::string CTABase::GetName()
{
    return ctaName;
}

CTAInterfaceBase *CTABase::GetLog()
{
    return log;
}

void CTABase::LogMessage(eMessageType Type, std::string Message)
{
    if (log != nullptr)
        log->LogMessage(this, Type, Message);
}
