#-------------------------------------------------
#
# Project created by QtCreator 2017-04-01T20:42:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CTAView
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        ctamain.cpp \
    Utils/CSVParser.cpp \
    CTA/ctabase.cpp \
    CTA/ctasimulator.cpp \
    CTA/ctapath.cpp \
    CTA/ctablock.cpp \
    CTA/ctacorner.cpp \
    CTA/ctacar.cpp \
    CTA/ctastreet.cpp \
    CTA/ctainterfacebase.cpp \
    QT/ctaqtinterface.cpp \
    QT/ctaqtmatrix.cpp \
    CTA/ctaeventhandlerbase.cpp \
    CTA/ctaeventhandleranimation.cpp \
    QT/ctaqteventhandler.cpp \
    CTA/ctacarbuffer.cpp

HEADERS  += ctamain.h \
    Utils/CSVParser.h \
    CTA/ctabase.h \
    CTA/ctasimulator.h \
    CTA/ctapath.h \
    CTA/ctablock.h \
    CTA/ctacorner.h \
    CTA/ctacar.h \
    CTA/ctastreet.h \
    CTA/ctadirection.h \
    CTA/messagetype.h \
    CTA/ctaevent.h \
    CTA/ctaorientation.h \
    CTA/ctaconstants.h \
    CTA/ctasemaphore.h \
    CTA/ctainterfacebase.h \
    QT/ctaqtinterface.h \
    QT/ctaqtmatrix.h \
    QT/ctaqtnodetype.h \
    QT/ctaqtcarcollor.h \
    QT/ctaqtsemaphorecollor.h \
    CTA/ctapathstatus.h \
    CTA/ctaeventhandleranimation.h \
    CTA/ctasimulationtype.h \
    CTA/ctaeventhandlerbase.h \
    QT/ctaqteventhandler.h \
    QT/ctasimulationstatus.h \
    QT/ctasimulationstatus.h \
    CTA/ctacarbuffer.h

FORMS    += ctamain.ui

DISTFILES += \
    Images/CarBottomUp.png \
    Images/CarBottomUp01.png \
    Images/CarBottomUp02.png \
    Images/CarBottomUp03.png \
    Images/CarBottomUp04.png \
    Images/CarBottomUp05.png \
    Images/CarLeftRight.png \
    Images/CarLeftRight01.png \
    Images/CarLeftRight02.png \
    Images/CarLeftRight03.png \
    Images/CarLeftRight04.png \
    Images/CarLeftRight05.png \
    Images/CarRightLeft.png \
    Images/CarRightLeft01.png \
    Images/CarRightLeft02.png \
    Images/CarRightLeft03.png \
    Images/CarRightLeft04.png \
    Images/CarRightLeft05.png \
    Images/CarTopDown.png \
    Images/CarTopDown01.png \
    Images/CarTopDown02.png \
    Images/CarTopDown03.png \
    Images/CarTopDown04.png \
    Images/CarTopDown05.png \
    Images/CornerGreen.png \
    Images/CornerLeftDiv.png \
    Images/CornerRed.png \
    Images/CornerRightDiv.png \
    Images/CornerYellow.png \
    Images/field.png \
    Images/HorStreet.png \
    Images/VerStreet.png

RESOURCES += \
    resources.qrc
