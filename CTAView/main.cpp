#include "ctamain.h"
#include <QApplication>
#include <QSemaphore>

#include "CTA/ctasimulator.h"
#include "QT/ctaqtinterface.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CTAMain w;
    QSemaphore semaphore(1);

    CTAQTInterface *myInterface = new CTAQTInterface(w.getScene(), w.getListWidget(), &semaphore);
    w.SetInterface(myInterface);

    w.show();
    return a.exec();
}
