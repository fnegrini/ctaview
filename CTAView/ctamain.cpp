#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QFileDialog>
#include <QString>

#include "ctamain.h"
#include "ui_ctamain.h"

#include "./QT/ctaqtinterface.h"
#include "./QT/ctaqteventhandler.h"
#include "./CTA/ctasimulator.h"
#include "./QT/ctaqteventhandler.h"

CTAMain::CTAMain(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CTAMain)
{
    ctaInterface = nullptr;
    ctaHandler = nullptr;
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(RefreshSimulation()));
    timer->start(100);

}

CTAMain::~CTAMain()
{
    delete scene;
    delete timer;
    delete ui;
}

QGraphicsScene *CTAMain::getScene() const
{
    return scene;
}

QListWidget *CTAMain::getListWidget() const
{
    return ui->listWidget;
}

void CTAMain::SetInterface(CTAQTInterface *CTAInterface)
{
    ctaInterface = CTAInterface;
}

void CTAMain::RefreshSimulation()
{
    if(ctaInterface!=nullptr){
        ctaInterface->RefreshMatrixQT();
        ctaInterface->RefreshMessagesQT();
    }
}

void CTAMain::on_btFile_clicked()
{
    ui->edtFile->setText(QFileDialog::getOpenFileName(this, tr("Open Simulation File"), ".",
                                                      tr("Text file (*.txt);; Any file (*.*)")));
}

void CTAMain::on_btnStart_clicked()
{
    CTAQTEventHandler *eventHandler = new CTAQTEventHandler((ui->edtFile->text().toStdString()), ui->spnClock->value());
    CTASimulator *simulator = new CTASimulator("Main Simulator", ctaInterface, eventHandler);

    ctaHandler = eventHandler;

    ui->listWidget->clear();
    scene->clear();

    try {

        simulator->StartSimulation();

    } catch (const std::exception&) {
        ctaHandler = nullptr;
        delete simulator;
        delete eventHandler;

    }

}

void CTAMain::on_btnZoomOut_clicked()
{
    ui->graphicsView->scale(0.75, 0.75);
}

void CTAMain::on_btnZoomIn_clicked()
{
    ui->graphicsView->scale(1.25, 1.25);
}

void CTAMain::on_btnPause_clicked()
{
    if(ctaHandler!= nullptr)
        ctaHandler->PauseSimulation();
}

void CTAMain::on_btnResume_clicked()
{
    if(ctaHandler!=nullptr)
        ctaHandler->ResumeSimulation();
}
