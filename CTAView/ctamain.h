#ifndef CTAMAIN_H
#define CTAMAIN_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QGraphicsScene>
#include <QTimer>
#include <QListWidget>

class CTAQTInterface;
class CTAQTEventHandler;

namespace Ui {
class CTAMain;
}

class CTAMain : public QDialog
{
    Q_OBJECT

public:
    explicit CTAMain(QWidget *parent = 0);
    ~CTAMain();

    QGraphicsScene *getScene() const;
    QListWidget *getListWidget() const;

    void SetInterface(CTAQTInterface *CTAInterface);

private slots:
    void RefreshSimulation();

    void on_btFile_clicked();

    void on_btnStart_clicked();

    void on_btnZoomOut_clicked();

    void on_btnZoomIn_clicked();

    void on_btnPause_clicked();

    void on_btnResume_clicked();

private:
    Ui::CTAMain *ui;
    QGraphicsScene *scene;
    CTAQTInterface *ctaInterface;
    CTAQTEventHandler *ctaHandler;
    QTimer *timer;

};

#endif // CTAMAIN_H
