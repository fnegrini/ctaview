#ifndef CTAQTEVENTHANDLER_H
#define CTAQTEVENTHANDLER_H

#include <QFile>

#include "../CTA/ctaeventhandlerbase.h"
#include "ctasimulationstatus.h"

#define EVT_INIT      "INIT"
#define EVT_CAR_NEW   "CAR-NEW"
#define EVT_CAR_MOVE  "CAR-MOVE"
#define EVT_CAR_EXIT  "CAR-EXIT"
#define EVT_CLOCK     "CLOCK"
#define EVT_SEMAPHORE "SEMAPHORE"
#define EVT_LOG       "LOG"
#define CHAR_SEP      '|'

class CTAQTEventHandler : public CTAEventHandlerBase
{
private:
    int horizontalCount;
    int verticalCount;
    int blockSize;
    int blockLanes;
    eSimulationType simulationType;
    eDirection hDirectionFirstStreet;
    eDirection vDirectionFirstStreet;
    unsigned long interval;
    unsigned int carSpeed;
    bool initOk;
    QFile *file;
    eSimulationStatus simulationStatus;

    void LoadFromFile(std::string FileName);
    void HandelEventCarNew(CTASimulator *simulator, QStringList Params);
    void HandelEventCarMove(CTASimulator *simulator, QStringList Params);
    void HandelEventCarExit(CTASimulator *simulator, QStringList Params);
    void HandelEventClock(CTASimulator *simulator, QStringList Params);
    void HandelEventSemaphore(CTASimulator *simulator, QStringList Params);
    void HandelEventLog(CTASimulator *simulator, QStringList Params);
    void CheckPaused();
protected:
    virtual CTAInitValues *getInitValuesInternal() override;
    virtual void HandleEventsInternal(CTASimulator *simulator) override;

public:
    CTAQTEventHandler(std::string FileName, unsigned long Interval);
    ~CTAQTEventHandler();

    bool PauseSimulation();
    bool ResumeSimulation();
    eSimulationStatus getSimulationStatus() const;
};

#endif // CTAQTEVENTHANDLER_H
