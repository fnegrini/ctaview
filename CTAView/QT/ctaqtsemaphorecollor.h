#ifndef CTAQTSEMAPHORECOLLOR_H
#define CTAQTSEMAPHORECOLLOR_H

enum eSemaphoreCollor {
    scNone = -1,
    scGreen = 0,
    scRed = 1,
    scYellow = 2,
};


#endif // CTAQTSEMAPHORECOLLOR_H
