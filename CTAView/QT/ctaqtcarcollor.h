#ifndef CTAQTCARCOLLOR_H
#define CTAQTCARCOLLOR_H

enum eCarCollor {
    ccBlue = 0,
    ccGreen = 1,
    ccPurple = 2,
    ccRed = 3,
    ccYellow = 4,
    ccBlack = 5,
};

#endif // CTAQTCARCOLLOR_H
