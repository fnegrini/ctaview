#include <string>
#include <iostream>

#include "ctaqteventhandler.h"
#include "../CTA/ctasimulator.h"


eSimulationStatus CTAQTEventHandler::getSimulationStatus() const
{
    return simulationStatus;
}

void CTAQTEventHandler::LoadFromFile(std::string FileName)
{
    QStringList initParam;
    QString line;

    file = new QFile(QString::fromStdString(FileName));

    if(file->exists()){
        file->open(QIODevice::ReadOnly|QIODevice::Text);
        line = file->readLine();
        initParam = line.split(CHAR_SEP);
        if((initParam.size()>0)&&(initParam[0]==EVT_INIT)){
            try {
                horizontalCount = initParam[1].toInt();
                verticalCount = initParam[2].toInt();
                simulationType = static_cast<eSimulationType>(initParam[7].toInt());
                hDirectionFirstStreet = static_cast<eDirection>(initParam[5].toInt());
                vDirectionFirstStreet = static_cast<eDirection>(initParam[6].toInt());
                blockSize = initParam[3].toInt();
                blockLanes = initParam[4].toInt();
                if(initParam.size()>8)
                    carSpeed = initParam[8].toInt();
                else
                    carSpeed = 1;
                initOk = true;

            } catch (const std::exception&) {
                initOk = false;
            }

        }
    }

}

void CTAQTEventHandler::HandelEventCarNew(CTASimulator *simulator, QStringList Params)
{
    simulator->HandleEventCarNew(Params[1].toInt(),
                                 static_cast<eStreetDirection>(Params[2].toInt()),
                                 Params[3].toInt());
}

void CTAQTEventHandler::HandelEventCarMove(CTASimulator *simulator, QStringList Params)
{

    if (Params.size() > 3){
        simulator->HandleEventCarMove(Params[1].toInt(), (Params[2].toInt()!=0), Params[3].toInt());
    }else{
        simulator->HandleEventCarMove(Params[1].toInt(), (Params[2].toInt()!=0));
    }

}

void CTAQTEventHandler::HandelEventCarExit(CTASimulator *simulator, QStringList Params)
{
    simulator->HandleEventCarExit(Params[1].toInt());
}

void CTAQTEventHandler::HandelEventClock(CTASimulator *simulator, QStringList Params)
{
    simulator->HandleEventClock(Params[1].toInt());
}

void CTAQTEventHandler::HandelEventSemaphore(CTASimulator *simulator, QStringList Params)
{
    simulator->HandleEventSemaphore(Params[1].toInt(), Params[2].toInt(),
            static_cast<eSemaphoreState>(Params[3].toInt()));
}

void CTAQTEventHandler::HandelEventLog(CTASimulator *simulator, QStringList Params)
{
    simulator->LogMessage(static_cast<eMessageType>(Params[1].toInt()), Params[2].toStdString());
}

void CTAQTEventHandler::CheckPaused()
{
    while (simulationStatus==sisPaused) {

    }
}

CTAInitValues *CTAQTEventHandler::getInitValuesInternal()
{
    if(initOk)
        return new CTAInitValues(horizontalCount, verticalCount, blockSize, blockLanes, simulationType,
                                 hDirectionFirstStreet, vDirectionFirstStreet, interval, carSpeed);
    else
        return nullptr;
}

void CTAQTEventHandler::HandleEventsInternal(CTASimulator *simulator)
{
    QStringList Param;
    QString line;
    int lineCount = 1;
    std::string message;

    if(!initOk) return;

    if(simulator==nullptr) return;

    while (!file->atEnd()) {
        CheckPaused();
        lineCount++;
        line = file->readLine();
        Param = line.split(CHAR_SEP);

        if(Param.size()>0){
            try {
                if(Param[0]==EVT_CAR_NEW){
                    HandelEventCarNew(simulator,Param);
                }else if(Param[0]==EVT_CAR_MOVE){
                    HandelEventCarMove(simulator,Param);
                }else if(Param[0]==EVT_CAR_EXIT){
                    HandelEventCarExit(simulator,Param);
                }else if(Param[0]==EVT_CLOCK){
                    HandelEventClock(simulator,Param);
                }else if(Param[0]==EVT_SEMAPHORE){
                    HandelEventSemaphore(simulator,Param);
                }else if(Param[0]==EVT_LOG){
                    HandelEventLog(simulator,Param);
                }else{
                    message = "Undentified event at line " + std::to_string(lineCount) + ": " + line.toStdString();
                    simulator->LogMessage(mtError, message);
                }
            } catch (const std::exception&) {
                message = "Wrong parameter at line " + std::to_string(lineCount) + ": " + line.toStdString();
                simulator->LogMessage(mtError, message);
            }


        }
    }

}

CTAQTEventHandler::CTAQTEventHandler(std::string FileName, unsigned long Interval)
{


    interval = Interval;
    file = nullptr;
    initOk = false;
    LoadFromFile(FileName);
    simulationStatus = sisNotRunning;

}

CTAQTEventHandler::~CTAQTEventHandler()
{
    if(file!=nullptr){
        if(file->isOpen())
            file->close();
        delete file;
    }
}

bool CTAQTEventHandler::PauseSimulation()
{
    if (simulationStatus == sisRunning){
        simulationStatus = sisPaused;
        return true;
    }else if (simulationStatus == sisPaused){
        // already paused
        return true;
    }else{
        return false;
    }
}

bool CTAQTEventHandler::ResumeSimulation()
{

    if (simulationStatus == sisPaused){
        simulationStatus = sisRunning;
        return true;
    }else if (simulationStatus == sisRunning){
        // already running
        return true;
    }else{
        return false;
    }
}
