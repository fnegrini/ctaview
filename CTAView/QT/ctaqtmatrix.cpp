#include "ctaqtmatrix.h"
#include "../CTA/ctacar.h"


int CTAQTMatrix::getHorCount() const
{
    return horCount;
}

void CTAQTMatrix::setHorCount(int value)
{
    horCount = value;
}

int CTAQTMatrix::getVerCount() const
{
    return verCount;
}

void CTAQTMatrix::setVerCount(int value)
{
    verCount = value;
}

void CTAQTMatrix::setNode(int Hor, int Ver, eNodeType Type, CTACar *Car)
{
    CTAQTNode *node = matrix[Hor][Ver];

    if (node  != nullptr){

        node->setType(Type);
        node->setCar(Car);

    }

}

CTAQTNode *CTAQTMatrix::getNode(int Hor, int Ver)
{
    return matrix[Hor][Ver];
}

CTAQTMatrix::CTAQTMatrix(int HorCount, int VerCount)
{
    matrix = new CTAQTNode**[HorCount];
    
    horCount = HorCount;
    verCount = VerCount;

    for (int iHor = 0; iHor < horCount; ++iHor) {
        matrix[iHor] = new CTAQTNode*[verCount];
        for (int iVer = 0; iVer < verCount; ++iVer) {
            matrix[iHor][iVer] = new CTAQTNode();
        }
    }

}

CTAQTMatrix::~CTAQTMatrix()
{
    CTAQTNode *item;

    for (int iHor = 0; iHor < horCount; ++iHor) {
        matrix[iHor] = new CTAQTNode*[verCount];
        for (int iVer = 0; iVer < verCount; ++iVer) {
            item = matrix[iHor][iVer];
            matrix[iHor][iVer] = nullptr;
            delete item;
        }
    }
}


CTACar *CTAQTNode::getCar() const
{
    return car;
}

void CTAQTNode::setCar(CTACar *value)
{
    if( car != value) setChanged(true);

    car = value;
}

eNodeType CTAQTNode::getType() const
{
    return type;
}

void CTAQTNode::setType(const eNodeType &value)
{
    if(type!=value) changed = true;

    type = value;
}


bool CTAQTNode::getChanged() const
{
    return changed;
}

void CTAQTNode::setChanged(bool value)
{
    changed = value;
}

eSemaphoreCollor CTAQTNode::getSemHorCollor() const
{
    return semHorCollor;
}

void CTAQTNode::setSemHorCollor(const eSemaphoreCollor &value)
{
    if(semHorCollor != value) setChanged(true);
    semHorCollor = value;
}

eSemaphoreCollor CTAQTNode::getSemVerCollor() const
{
    return semVerCollor;
}

void CTAQTNode::setSemVerCollor(const eSemaphoreCollor &value)
{
    if(semVerCollor != value) setChanged(true);
    semVerCollor = value;
}

eDirection CTAQTNode::getHorDirection() const
{
    return horDirection;
}

void CTAQTNode::setHorDirection(const eDirection &value)
{
    horDirection = value;
}

eDirection CTAQTNode::getVerDirection() const
{
    return verDirection;
}

void CTAQTNode::setVerDirection(const eDirection &value)
{
    verDirection = value;
}

CTAQTNode::CTAQTNode(eNodeType Type, CTACar *Car)
{
    type = Type;
    car = Car;
    changed = true;
    semHorCollor = scNone;
    semVerCollor = scNone;
}
