#ifndef CTAQTNODETYPE_H
#define CTAQTNODETYPE_H

enum eNodeType {

    ntField = 0,
    ntStreetHor = 1,
    ntStreetVer = 2,
    ntCorner = 3,

};

#endif // CTAQTNODETYPE_H
