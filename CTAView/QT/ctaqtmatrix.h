#ifndef CTAMATRIX_H
#define CTAMATRIX_H


#include "ctaqtnodetype.h"
#include "ctaqtcarcollor.h"
#include "ctaqtsemaphorecollor.h"
#include "../CTA/ctadirection.h"


class CTACar;


class CTAQTNode
{
private:
    eNodeType type;
    bool changed;
    CTACar *car;
    eSemaphoreCollor semHorCollor;
    eSemaphoreCollor semVerCollor;

    eDirection horDirection;
    eDirection verDirection;

public:
    CTAQTNode(eNodeType Type = ntField, CTACar *Car = nullptr);

    eNodeType getType() const;
    void setType(const eNodeType &value);

    bool getChanged() const;
    void setChanged(bool value);

    CTACar *getCar() const;
    void setCar(CTACar *value);

    eSemaphoreCollor getSemHorCollor() const;
    void setSemHorCollor(const eSemaphoreCollor &value);

    eSemaphoreCollor getSemVerCollor() const;
    void setSemVerCollor(const eSemaphoreCollor &value);
    eDirection getHorDirection() const;
    void setHorDirection(const eDirection &value);
    eDirection getVerDirection() const;
    void setVerDirection(const eDirection &value);
};

class CTAQTMatrix
{
private:
    CTAQTNode*** matrix;
    int horCount;
    int verCount;

public:
    CTAQTMatrix(int HorCount = 0, int VerCount = 0);
    ~CTAQTMatrix();

    int getHorCount() const;
    void setHorCount(int value);
    int getVerCount() const;
    void setVerCount(int value);
    void setNode(int Hor, int Ver, eNodeType Type, CTACar *Car = nullptr);
    CTAQTNode *getNode(int Hor, int Ver);
};

#endif // CTAMATRIX_H
