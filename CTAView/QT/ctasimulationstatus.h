#ifndef CTASIMULATIONSTATUS_H
#define CTASIMULATIONSTATUS_H

enum eSimulationStatus {
    sisNotRunning,
    sisRunning,
    sisPaused,
    sisFinished,
};
#endif // CTASIMULATIONSTATUS_H
