#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QObject>
#include <QGraphicsView>

#include "ctaqtinterface.h"
#include "ctaqtmatrix.h"
#include "../CTA/ctaconstants.h"
#include "../CTA/ctadirection.h"
#include "../CTA/ctacar.h"
#include "../CTA/ctapath.h"
#include "../CTA/ctacorner.h"

bool CTAQTInterface::getMatrixChanged() const
{
    return matrixChanged;
}

void CTAQTInterface::InitializeGraphMatrix(int HorizontalCount, int VerticalCount, int BlockSize, int BlockLanes)
{
    int ver, hor;

    ver = ( BlockSize * (HorizontalCount+1)) + (BlockLanes * HorizontalCount);
    hor = ( BlockSize * (VerticalCount+1)) + (BlockLanes * VerticalCount);

    matrix = new CTAQTMatrix(hor, ver);

    InitializePixmaps(hor, ver);
    InitializeGraphFields();
    InitializeGraphVerBlocks();
    InitializeGraphHorBlocks();
    InitializeGraphCorners();
}

void CTAQTInterface::InitializeGraphFields()
{
    for (int iHor = 0; iHor < matrix->getHorCount(); ++iHor) {
        for (int iVer = 0; iVer < matrix->getVerCount(); ++iVer) {
            matrix->setNode(iHor, iVer, ntField);
        }
    }
}

void CTAQTInterface::InitializeGraphHorBlocks()
{
    int VerticalSplit;
    int HorizontalSplit;

    for (int iVer = 0; iVer < getVerticalCount(); ++iVer) {

        //Determine vertical split
        HorizontalSplit = ((iVer+1)*getBlockSize()) + (iVer*getBlockLanes());

        for (int iBlock = 0; iBlock < (getHorizontalCount() + 1); ++iBlock) {

            //Determine horizontal split
            VerticalSplit = iBlock * (getBlockSize() + getBlockLanes());

            for (int iPathHor = 0; iPathHor < getBlockLanes(); ++iPathHor) {

                for (int iPathVer = 0; iPathVer < getBlockSize(); ++iPathVer) {

                    matrix->setNode(HorizontalSplit+iPathHor, VerticalSplit+iPathVer, ntStreetVer);

                }
            }
        }
    }

}

void CTAQTInterface::InitializeGraphVerBlocks()
{
    int VerticalSplit;
    int HorizontalSplit;

    for (int iHor = 0; iHor < getHorizontalCount(); ++iHor) {

        //Determine vertical split
        VerticalSplit = ((iHor+1)*getBlockSize()) + (iHor*getBlockLanes());

        for (int iBlock = 0; iBlock < (getVerticalCount() + 1); ++iBlock) {

            //Determine horizontal split
            HorizontalSplit = iBlock * (getBlockSize() + getBlockLanes());

            for (int iPathHor = 0; iPathHor < getBlockSize(); ++iPathHor) {

                for (int iPathVer = 0; iPathVer < getBlockLanes(); ++iPathVer) {

                    matrix->setNode(HorizontalSplit+iPathHor, VerticalSplit+iPathVer, ntStreetHor);

                }
            }
        }
    }

}

void CTAQTInterface::InitializeGraphCorners()
{
    int HorizontalSplit, VerticalSplit;

    for (int iHor = 0; iHor < getHorizontalCount(); ++iHor) {

        for (int iVer = 0; iVer < getVerticalCount(); ++iVer) {

            HorizontalSplit = (iHor+1)*getBlockSize() + (iHor*getBlockLanes());
            VerticalSplit = (iVer+1)*getBlockSize() + (iVer*getBlockLanes());

            for (int iHorPath = 0; iHorPath < getBlockLanes(); ++iHorPath) {

                for (int iVerPath = 0; iVerPath < getBlockLanes(); ++iVerPath) {

                    matrix->setNode(HorizontalSplit+iHorPath, VerticalSplit+iVerPath, ntCorner);

                }

            }
        }
    }

}

void CTAQTInterface::InitializePixmaps(int Hor, int Ver)
{
    QGraphicsPixmapItem *item;
    horCount = Hor;
    verCount = Ver;

    pixmap = new QGraphicsPixmapItem**[horCount];

    for (int iHor = 0; iHor < horCount; ++iHor) {
        pixmap[iHor] = new QGraphicsPixmapItem*[verCount];
        for (int iVer = 0; iVer < verCount; ++iVer) {
            item = scene->addPixmap(QPixmap::fromImage(EmtpyImage));
            item->setPos(iHor*IMAGE_SIZE_HOR, iVer*IMAGE_SIZE_VER);
            pixmap[iHor][iVer] = item;
        }
    }

}

void CTAQTInterface::RefreshMatrixInternal()
{
    matrixChanged = true;
}

void CTAQTInterface::LogMessageInternal(CTABase *Object, eMessageType Type, std::string Message)
{
    semaphore->acquire();
    messageList.push_front(new CTAQTLogMessage(Object, Type, Message));
    semaphore->release();
}

void CTAQTInterface::RefreshNode(int Hor, int Ver, CTAQTNode *node)
{
    QImage result(IMAGE_SIZE_HOR, IMAGE_SIZE_VER, QImage::Format_ARGB32);
    QPainter painter(&result);

    PCTACar car = node->getCar();

    painter.drawImage(0, 0, images[node->getType()]);

    if (car != nullptr){
        painter.drawImage(0, 0, getCarImage(car));
    }

    if(node->getSemHorCollor()!= scNone){
        painter.drawImage(0, 0, semHorImages[node->getHorDirection()][node->getSemHorCollor()]);
    }

    if(node->getSemVerCollor()!= scNone){
        painter.drawImage(0, 0, semVerImages[node->getVerDirection()][node->getSemVerCollor()]);
    }
    pixmap[Hor][Ver]->setPixmap(QPixmap::fromImage(result));

}



QImage CTAQTInterface::getCarImage(CTACar *Car)
{
    return carImages[(Car->getCarID() % 6) ][Car->getDirection()];
}

void CTAQTInterface::InitializeMatrixInternal(int horizontalCount, int VerticalCount, int BlockSize, int BlockLanes)
{
   InitializeGraphMatrix(horizontalCount, VerticalCount, BlockSize, BlockLanes);
   scene->setSceneRect(0,0,matrix->getHorCount()*IMAGE_SIZE_HOR, matrix->getVerCount()*IMAGE_SIZE_VER);
   RefreshMatrixInternal();
   MessageCount = 0;
}

void CTAQTInterface::SetCarToPathInternal(CTAPath *path, int Hor, int Ver, CTACar *Car)
{
    semaphore->acquire();
    matrix->getNode(path->getHorOffSet()+Hor, path->getVerOffSet()+Ver)->setCar(Car);
    matrixChanged = true;
    semaphore->release();
}

void CTAQTInterface::SetCornerInternal(CTACorner *corner)
{
    eSemaphoreCollor HCollor, VCollor;
    CTAQTNode *node;

    switch (corner->getSemaphore()) {

    // Determine semaphore collors
    case ssNone:
        HCollor = scNone;
        VCollor = scNone;
        break;

    case ssHorRedVerRedNextHor:
        HCollor = scRed;
        VCollor = scRed;
        break;

    case ssHorGreenVerRed:
        HCollor = scGreen;
        VCollor = scRed;
        break;

    case ssHorYellowVerRed:
        HCollor = scYellow;
        VCollor = scRed;
        break;

    case ssHorRedVerRedNextVer:
        HCollor = scRed;
        VCollor = scRed;
        break;

    case ssHorRedVerGreen:
        HCollor = scRed;
        VCollor = scGreen;
        break;

    case ssHorRedVerYellow:
        HCollor = scRed;
        VCollor = scYellow;
        break;
    }

    switch (corner->getHorDirection()) {
    case gdBeginToEnd:
        for (int i = 0; i < corner->getVerCount(); ++i) {
            node = matrix->getNode(corner->getHorOffSet(), corner->getVerOffSet()+i);
            node->setSemHorCollor(HCollor);
            node->setHorDirection(gdBeginToEnd);
        }
        break;

    case gdEndToBegin:
        for (int i = 0; i < corner->getVerCount(); ++i) {
            node = matrix->getNode(corner->getHorOffSet()+corner->getHorCount()-1, corner->getVerOffSet()+i);
            node->setSemHorCollor(HCollor);
            node->setHorDirection(gdEndToBegin);
        }
        break;

    case gdNone:
        for (int iHor = 0; iHor < corner->getHorCount(); ++iHor) {
            for (int iVer = 0; iVer < corner->getVerCount(); ++iVer) {
                node = matrix->getNode(corner->getHorOffSet()+iHor, corner->getVerOffSet()+iVer);
                node->setSemHorCollor(scNone);
                node->setHorDirection(gdNone);
            }
        }
    }


    switch (corner->getVerDirection()) {
    case gdBeginToEnd:
        for (int i = 0; i < corner->getHorCount(); ++i) {
            node = matrix->getNode(corner->getHorOffSet()+i, corner->getVerOffSet());
            node->setSemVerCollor(VCollor);
            node->setVerDirection(gdBeginToEnd);
        }
        break;

    case gdEndToBegin:
        for (int i = 0; i < corner->getHorCount(); ++i) {
            node = matrix->getNode(corner->getHorOffSet()+i, corner->getVerOffSet()+corner->getVerCount()-1);
            node->setSemVerCollor(VCollor);
            node->setVerDirection(gdEndToBegin);
        }
        break;

    case gdNone:
        for (int iHor = 0; iHor < corner->getHorCount(); ++iHor) {
            for (int iVer = 0; iVer < corner->getVerCount(); ++iVer) {
                node = matrix->getNode(corner->getHorOffSet()+iHor, corner->getVerOffSet()+iVer);
                node->setSemVerCollor(scNone);
                node->setVerDirection(gdNone);
            }
        }
    }

}


CTAQTInterface::CTAQTInterface(QGraphicsScene *Scene, QListWidget *ListWidget, QSemaphore *Semaphore)
    :CTAInterfaceBase()
{
    scene = Scene;
    semaphore = Semaphore;
    listWidget = ListWidget;

    // Map Images

    // ntField = 0
    images[0] = QImage(":/QT/Images/Field.png\0");

    //ntStreetHor = 1
    images[1] = QImage(":/QT/Images/HorStreet.png\0");

    //ntStreetVer = 2
    images[2] = QImage(":/QT/Images/VerStreet.png\0");

    //ntCorner = 3
    images[3] = QImage(":/QT/Images/Corner.png\0");

    EmtpyImage = QImage(":/QT/Images/Empty.png\0");


    // Car Images
    carImages[ccBlue][cdUpDown]    = QImage(":/QT/Images/Car/CarDownBlue.png\0");
    carImages[ccBlue][cdBottomUp]  = QImage(":/QT/Images/Car/CarUpBlue.png\0");
    carImages[ccBlue][cdLeftRight] = QImage(":/QT/Images/Car/CarRightBlue.png\0");
    carImages[ccBlue][cdRightLeft] = QImage(":/QT/Images/Car/CarLeftBlue.png\0");

    carImages[ccGreen][cdUpDown]    = QImage(":/QT/Images/Car/CarDownGreen.png\0");
    carImages[ccGreen][cdBottomUp]  = QImage(":/QT/Images/Car/CarUpGreen.png\0");
    carImages[ccGreen][cdLeftRight] = QImage(":/QT/Images/Car/CarRightGreen.png\0");
    carImages[ccGreen][cdRightLeft] = QImage(":/QT/Images/Car/CarLeftGreen.png\0");

    carImages[ccPurple][cdUpDown]    = QImage(":/QT/Images/Car/CarDownPurple.png\0");
    carImages[ccPurple][cdBottomUp]  = QImage(":/QT/Images/Car/CarUpPurple.png\0");
    carImages[ccPurple][cdLeftRight] = QImage(":/QT/Images/Car/CarRightPurple.png\0");
    carImages[ccPurple][cdRightLeft] = QImage(":/QT/Images/Car/CarLeftPurple.png\0");

    carImages[ccRed][cdUpDown]    = QImage(":/QT/Images/Car/CarDownRed.png\0");
    carImages[ccRed][cdBottomUp]  = QImage(":/QT/Images/Car/CarUpRed.png\0");
    carImages[ccRed][cdLeftRight] = QImage(":/QT/Images/Car/CarRightRed.png\0");
    carImages[ccRed][cdRightLeft] = QImage(":/QT/Images/Car/CarLeftRed.png\0");

    carImages[ccYellow][cdUpDown]    = QImage(":/QT/Images/Car/CarDownYellow.png\0");
    carImages[ccYellow][cdBottomUp]  = QImage(":/QT/Images/Car/CarUpYellow.png\0");
    carImages[ccYellow][cdLeftRight] = QImage(":/QT/Images/Car/CarRightYellow.png\0");
    carImages[ccYellow][cdRightLeft] = QImage(":/QT/Images/Car/CarLeftYellow.png\0");

    carImages[ccBlack][cdUpDown]    = QImage(":/QT/Images/Car/CarDownBlack.png\0");
    carImages[ccBlack][cdBottomUp]  = QImage(":/QT/Images/Car/CarUpBlack.png\0");
    carImages[ccBlack][cdLeftRight] = QImage(":/QT/Images/Car/CarRightBlack.png\0");
    carImages[ccBlack][cdRightLeft] = QImage(":/QT/Images/Car/CarLeftBlack.png\0");

    // Semaphore images
    semVerImages[gdEndToBegin][scGreen]  = QImage(":/QT/Images/Semaphore/SemDownGreen.png\0");
    semVerImages[gdEndToBegin][scRed]    = QImage(":/QT/Images/Semaphore/SemDownRed.png\0");
    semVerImages[gdEndToBegin][scYellow] = QImage(":/QT/Images/Semaphore/SemDownYellow.png\0");

    semVerImages[gdBeginToEnd][scGreen]  = QImage(":/QT/Images/Semaphore/SemUpGreen.png\0");
    semVerImages[gdBeginToEnd][scRed]    = QImage(":/QT/Images/Semaphore/SemUpRed.png\0");
    semVerImages[gdBeginToEnd][scYellow] = QImage(":/QT/Images/Semaphore/SemUpYellow.png\0");

    semHorImages[gdEndToBegin][scGreen]  = QImage(":/QT/Images/Semaphore/SemRightGreen.png\0");
    semHorImages[gdEndToBegin][scRed]    = QImage(":/QT/Images/Semaphore/SemRightRed.png\0");
    semHorImages[gdEndToBegin][scYellow] = QImage(":/QT/Images/Semaphore/SemRightYellow.png\0");

    semHorImages[gdBeginToEnd][scGreen]  = QImage(":/QT/Images/Semaphore/SemLeftGreen.png\0");
    semHorImages[gdBeginToEnd][scRed]    = QImage(":/QT/Images/Semaphore/SemLeftRed.png\0");
    semHorImages[gdBeginToEnd][scYellow] = QImage(":/QT/Images/Semaphore/SemLeftYellow.png\0");





}

void CTAQTInterface::RefreshMatrixQT()
{
    CTAQTNode *node;

    if(!matrixChanged) return;

    semaphore->acquire();
    for (int iHor = 0; iHor < matrix->getHorCount(); ++iHor) {
        for (int iVer = 0; iVer < matrix->getVerCount(); ++iVer) {
            node = matrix->getNode(iHor, iVer);
            if(node != nullptr){
                if (node->getChanged()){
                    RefreshNode(iHor, iVer, node);
                    node->setChanged(false);
                }
            }
        }
    }
    semaphore->release();

}

void CTAQTInterface::RefreshMessagesQT()
{
    PCTAQTLogMessage message;
    std::string message_out;
    QListWidgetItem *item;

    if (messageList.size()==0) return;

    semaphore->acquire();
    while (messageList.size()>0) {
        message = messageList.back();

        message_out = message->getObject()->GetName() + "[" + message->getObject()->GetClassName() + "] ";

        switch (message->getType()) {
        case mtSuccess:
            message_out = message_out + "SUCCESS: " + message->getMessage();
            break;

        case mtWarning:
            message_out = message_out + "WARNING: " + message->getMessage();
            break;

        case mtError:
            message_out = message_out + "ERROR  : " + message->getMessage();
            break;

        case mtDebug:
            message_out = message_out + "DEBUG  : " + message->getMessage();
            break;

        }

        item = new QListWidgetItem(listWidget);
        item->setText(QString::fromStdString(message_out));
        listWidget->addItem(item);
        listWidget->setCurrentRow(MessageCount);
        MessageCount++;

        messageList.pop_back();
        delete message;
    }
    semaphore->release();

}


CTAQTInterface::~CTAQTInterface()
{
    // Destroy matrix
    delete matrix;
}

eMessageType CTAQTLogMessage::getType() const
{
    return type;
}

std::string CTAQTLogMessage::getMessage() const
{
    return message;
}

CTAQTLogMessage::CTAQTLogMessage(CTABase *Object, eMessageType Type, std::string Message)
{
    object = Object;
    type = Type;
    message = Message;
}

CTABase *CTAQTLogMessage::getObject() const
{
    return object;
}
