#ifndef CTAQTINTERFACE_H
#define CTAQTINTERFACE_H


#include <QGraphicsScene>
#include <QImage>
#include <QSemaphore>
#include <QListWidget>

#include <list>

#include "../CTA/ctainterfacebase.h"
#include "ctaqtmatrix.h"

#define IMAGE_SIZE_HOR 20
#define IMAGE_SIZE_VER 20

class CTAQTLogMessage
{
private:
    CTABase *object;
    eMessageType type;
    std::string message;
public:
    CTAQTLogMessage(CTABase *Object, eMessageType Type, std::string Message);

    CTABase *getObject() const;
    eMessageType getType() const;
    std::string getMessage() const;
};

typedef CTAQTLogMessage *PCTAQTLogMessage;
typedef std::list<PCTAQTLogMessage> CTAQTLogMessageList;

class CTAQTInterface : public CTAInterfaceBase
{
private:
    CTAQTMatrix *matrix;

    QGraphicsScene *scene;
    QSemaphore *semaphore;
    QListWidget *listWidget;

    QImage images[4];
    QImage carImages[6][4];
    QImage semHorImages[2][3];
    QImage semVerImages[2][3];
    QImage EmtpyImage;

    QGraphicsPixmapItem ***pixmap;

    CTAQTLogMessageList messageList;

    int horCount;
    int verCount;

    bool matrixChanged = false;
    int MessageCount;

    void InitializeGraphMatrix(int HorizontalCount, int VerticalCount, int BlockSize, int BlockLanes);
    void InitializeGraphFields();
    void InitializeGraphHorBlocks();
    void InitializeGraphVerBlocks();

    void InitializeGraphCorners();
    void InitializePixmaps(int Hor, int Ver);

    void RefreshNode(int Hor, int Ver, CTAQTNode *node);

    QImage getCarImage(CTACar *Car);

protected:
    virtual void LogMessageInternal(CTABase *Object, eMessageType Type, std::string Message);
    virtual void InitializeMatrixInternal(int horizontalCount, int VerticalCount, int BlockSize, int BlockLanes);
    virtual void SetCarToPathInternal(CTAPath *path, int Hor, int Ver, CTACar *Car);
    virtual void SetCornerInternal(CTACorner *corner);
    virtual void RefreshMatrixInternal();

public:
     CTAQTInterface(QGraphicsScene *Scene = nullptr,  QListWidget *ListWidget = nullptr, QSemaphore *Semaphore = nullptr);
     ~CTAQTInterface();

     void RefreshMatrixQT();
     void RefreshMessagesQT();
     bool getMatrixChanged() const;
};

#endif // CTAQTINTERFACE_H
